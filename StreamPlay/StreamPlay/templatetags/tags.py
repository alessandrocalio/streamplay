from django import template

from orders.views import ShoppingCart

register = template.Library()


@register.simple_tag
def item_in_cart(request):
    user_cart = ShoppingCart.objects.get(UserId=request.user.id)
    return user_cart.get_item_num()
