import calendar
import json

from django.contrib.admin.views.decorators import staff_member_required
from django.core import serializers
from django.db.models import Count
from django.shortcuts import render
from django.template import RequestContext

from games_management.models import Videogame
from orders.models import Order
from raccomander_systems.hybrid_algorithm.algorithm import get_videogames


def get_names():
    object_list = Videogame.objects.all().order_by('name')
    serialized_objects = serializers.serialize('json', object_list)
    json_objects = json.loads(serialized_objects)
    list_objects = []
    for index in range(len(json_objects)):
        list_objects.append(json_objects[index]['fields']['name'])
    return list_objects


def home(request):
    context = {}
    # i 6 videogame più venduti al momento di sempre

    context['vg_names'] = get_names()

    new_games = Videogame.objects.all().order_by('-release_date')[:6]
    context['new_games'] = new_games[1:6]
    context['newest_game'] = new_games[0]

    # i 6 videogame più venduti al momento
    query_m_s_vg = Order.objects.all().values('Videogames__name').annotate(total=Count('Videogames__name')).order_by(
        '-total')[:6]
    most_sold_videogames = []
    for game in query_m_s_vg:
        most_sold_videogames.append(Videogame.objects.get(name=game['Videogames__name']))

    context['most_sold_videogames'] = [most_sold_videogames[3:], most_sold_videogames[:3]]

    # le instanze dei videogame ordinati per similarity*predicted_rate
    if (request.user.is_authenticated):
        raccomended_videogames_general = get_videogames(userId=request.user.id)
        if raccomended_videogames_general.__len__() >= 6:
            context['raccomended_videogames'] = [raccomended_videogames_general[3:6],
                                                 raccomended_videogames_general[:3]]
        else:
            context['raccomended_videogames'] = []
    # games with higher average reviews
    avg_games = Videogame.objects.all().order_by('-avg_review')[:6]
    context['avg_games'] = [avg_games[3:], avg_games[:3]]

    return render(request, 'home.html', context=context)


@staff_member_required
def stats(request):
    context = dict()
    context['vg_names'] = get_names()

    name_videogame = request.GET.get('name')
    month = request.GET.get('month')

    if name_videogame:
        if month != '00':
            game_query = Order.objects.filter(Date__month=month,
                                              Videogames__name=name_videogame).values('Date').annotate(
                total=Count('Date')).order_by('Date')
            context[
                'graph_name'] = f'{name_videogame} sales in {calendar.month_name[int(month)]}'
            if game_query:
                context['game_not_found'] = False
            else:
                context['game_not_found'] = True

        else:
            game_query = Order.objects.filter(Videogames__name=name_videogame).values('Date').annotate(
                total=Count('Date')).order_by('Date')
            context['graph_name'] = f'{name_videogame} all time sales'
            if game_query:
                context['game_not_found'] = False
            else:
                context['game_not_found'] = True

        game_query_dict = dict()

        for el in game_query.values('Date', 'total'):
            game_query_dict[str(el['Date'])] = el['total']

        context['game_query'] = game_query_dict

    elif month and month != '00':

        game_query = Order.objects.filter(Date__month=month).values('Videogames__name').annotate(
            total=Count('Videogames__name')).order_by(
            '-total')[:25]
        game_query_dict = dict()

        for el in game_query.values('Videogames__name', 'total'):
            game_query_dict[str(el['Videogames__name'])] = el['total']
        context['graph_name'] = f'Top 25 best sellers - {calendar.month_name[int(month)]}'
        context['game_query'] = game_query_dict

    else:
        game_query = Order.objects.all().values('Videogames__name').annotate(total=Count('Videogames__name')).order_by(
            '-total')[:25]
        game_query_dict = dict()
        for el in game_query.values('Videogames__name', 'total'):
            game_query_dict[str(el['Videogames__name'])] = el['total']
        context['graph_name'] = f'Top 25 best sellers - All time'
        context['game_query'] = game_query_dict

    return render(request, 'stats.html', context=context)
