from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from StreamPlay import views
from raccomander_systems.reviews_filter.videogame_by_reviews import fit_algo, create_model

urlpatterns = [
                  path('user_management/', include('user_management.urls')),
                  path('games_management/', include('games_management.urls')),
                  path('orders/', include('orders.urls')),
                  path('admin/', admin.site.urls),
                  path('stats/', views.stats, name='stats_page'),
                  path('', views.home,name='home'),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#create_model()
fit_algo()
