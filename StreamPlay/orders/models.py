import random
from datetime import datetime, timedelta

from django.db import models

from games_management.models import Videogame
from user_management.models import User

'''
Used for populate db
'''
def random_date():
    """
    This function will return a random datetime between two datetime
    objects.
    """
    start = datetime.strptime('1/1/2020 1:30 PM', '%m/%d/%Y %I:%M %p')
    end = datetime.strptime('9/1/2020 4:50 AM', '%m/%d/%Y %I:%M %p')
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)


class Order(models.Model):
    UserId = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    Videogames = models.ManyToManyField(Videogame)
    Date = models.DateField(auto_now=True)
    TotalPrice = models.DecimalField(max_digits=7, decimal_places=2)

    @classmethod
    def create(cls, userid, subTotal):
        order = cls(UserId=userid)
        order.TotalPrice = subTotal
        return order

    '''
    Used for populate db
    '''

    @classmethod
    def create2(cls, userid, subTotal):
        order = cls(UserId=userid)
        order.TotalPrice = subTotal
        order.Date = (random_date())
        return order

    def addVideogames(self, Videogames):
        for videogame in Videogames:
            self.Videogames.add(videogame)

    def addVideogame(self, Videogame):
        self.Videogames.add(Videogame)

    def getVideogames(self):
        return self.Videogames.all()


class ShoppingCart(models.Model):
    UserId = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    Videogames = models.ManyToManyField(Videogame, blank=True)


    '''
    Used for testing
    '''
    def add_videogame_o(self, videogame):
        self.Videogames.add(videogame)

    def add_videogame(self, videogame_name):
        videogame_selected = Videogame.objects.get(name=videogame_name)
        self.Videogames.add(videogame_selected)

    def remove_videogame(self, videogame_name):
        videogame_selected = Videogame.objects.get(name=videogame_name)
        self.Videogames.remove(videogame_selected)

    def empty(self):
        self.Videogames.clear()

    def get_videogames(self):
        pass

    @classmethod
    def create(cls, userid):
        shopping_cart = cls(UserId=userid)
        return shopping_cart

    def get_item_num(self):
        return self.Videogames.count()

    def sub_total(self):
        videogame_list = self.Videogames.all()
        subtotal = 0
        for videogame in videogame_list:
            if videogame.on_sale:
                subtotal = subtotal + videogame.get_discounted_price()
            else:
                subtotal = subtotal + videogame.price

        return subtotal

    def in_cart(self, videogame_name):
        videogame_selected = Videogame.objects.get(name=videogame_name)
        for v in self.Videogames.all():
            if videogame_selected == v:
                return True
        return False


class Library(models.Model):
    UserId = models.OneToOneField(User, on_delete=models.CASCADE)
    Videogames = models.ManyToManyField(Videogame, blank=True)

    @classmethod
    def create(cls, userid):
        Library = cls(UserId=userid)
        return Library

    def addVideogames(self, Videogames):
        for videogame in Videogames:
            self.Videogames.add(videogame)

    def getVideogames(self):
        return self.Videogames.all()

    def add_videogame(self, videogame):
        self.Videogames.add(videogame)

    def in_Library(self, videogame_name):
        videogame_selected = Videogame.objects.get(name=videogame_name)
        for v in self.Videogames.all():
            if videogame_selected == v:
                return True
        return False
