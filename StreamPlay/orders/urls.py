from django.urls import path
from . import views

urlpatterns = [
    path('add/<str:name>/', views.add_cart, name='add-cart'),
    path('empty/', views.empty_cart, name='empty-cart'),
    path('remove/<str:name>',views.remove_cart,name='remove-cart'),
    path('', views.cart, name='cart'),
    path('checkout', views.checkout, name='checkout')
]