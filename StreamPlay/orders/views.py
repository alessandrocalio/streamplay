from background_task import background
from compat import render_to_string
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage
from django.shortcuts import redirect, render
from django.urls import resolve

from user_management.models import User
from .models import ShoppingCart, Order, Library


@login_required(login_url='/user_management/login')
def add_cart(request, name):
    user_cart = ShoppingCart.objects.get(UserId=request.user.id)
    library = Library.objects.get(UserId=request.user.id)
    if user_cart.in_cart(videogame_name=name) or library.in_Library(videogame_name=name):
        return redirect('cart')
    user_cart.add_videogame(videogame_name=name)
    return redirect('cart')


@login_required(login_url='/user_management/login')
def remove_cart(request, name):
    user_cart = ShoppingCart.objects.get(UserId=request.user.id)
    user_cart.remove_videogame(videogame_name=name)
    return redirect('cart')


@login_required(login_url='/user_management/login')
def empty_cart(request):
    user_cart = ShoppingCart.objects.get(UserId=request.user.id)
    user_cart.empty()
    return redirect('cart')


@login_required(login_url='/user_management/login')
def cart(request):
    user_cart = ShoppingCart.objects.get(UserId=request.user.id)
    context = {'user_cart': user_cart,
               }
    return render(request, 'orders/cart.html', context)


@login_required(login_url='/user_management/login')
def checkout(request):
    if request.method == "GET":
        return redirect('cart')

    user_cart = ShoppingCart.objects.get(UserId=request.user.id)
    user_library = Library.objects.get(UserId=request.user.id)

    if user_cart.Videogames.count() <= 0:
        return redirect('cart')

    order = Order.create(userid=request.user, subTotal=user_cart.sub_total())
    order.save()

    order.addVideogames(user_cart.Videogames.all())
    user_library.addVideogames(user_cart.Videogames.all())

    order.save()
    user_cart.empty()
    user_library.save()

    checkout_email(order.id, request.user.id)
    return render(request, 'orders/success_checkout.html')


@background(schedule=60)
def checkout_email(order_id, user_id):
    user = User.objects.get(id=user_id)
    order = Order.objects.get(id=order_id)
    to_email = user.email
    mail_subject = '[StreamPlay ORDER N°' + str(order.id) + ']'
    message = render_to_string('orders/checkout_e.html', {
        'user': user.username,
        'videogames': order.getVideogames(),
        'total': order.TotalPrice,
    })
    email = EmailMessage(
        mail_subject, message, to=[to_email]
    )
    email.send()
