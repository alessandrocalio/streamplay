from django.contrib import admin

from user_management.models import PrivacyField
from .models import Order , ShoppingCart

# Register your models here.

admin.site.register(Order)
admin.site.register(ShoppingCart)
admin.site.register(PrivacyField)

