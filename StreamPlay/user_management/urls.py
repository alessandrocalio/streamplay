from django.contrib.auth import views as auth_views
from django.urls import path

from user_management import views

urlpatterns = [
    path('registration', views.registration, name='registration'),
    path('logout', views.logoutview, name='logout'),
    path('login', views.loginview, name='login'),
    path('profile/<slug>', views.Profile.as_view(), name='user-details'),
    path('profile/update/', views.UpdateProfile, name='update'),
    path('activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/',
         views.activate, name='activate'),
    path("password_reset", views.password_reset_request, name="password_reset"),
    path('password_reset/done/',
         auth_views.PasswordResetDoneView.as_view(template_name='user_management/password_reset_done.html'),
         name='password_reset_done'),
    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name='user_management/password_reset_confirm.html'),
         name='password_reset_confirm'),
    path('reset/done/',
         auth_views.PasswordResetCompleteView.as_view(template_name='user_management/password_reset_complete.html'),
         name='password_reset_complete'),

]
