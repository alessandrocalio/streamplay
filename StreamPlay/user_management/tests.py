from datetime import datetime

from django.test import TestCase, RequestFactory
from django.urls import reverse

from games_management.models import Videogame
from orders.models import ShoppingCart, Library
from user_management.models import User, PrivacyField


class ViewsTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='jacob', email='jacob@mail.com', password='top_secret')
        self.user.is_active = True
        self.user.save()

        shopping_cart = ShoppingCart.create(userid=self.user)
        shopping_cart.save()

        library = Library.create(userid=self.user)
        library.save()

        privacy = PrivacyField.create(userid=self.user)
        privacy.save()

        # Popolazione db test

        self.game_1 = Videogame.create(name="Test1", price=10, releasedate=datetime.now(), genre="RPG", sid=1000)
        self.game_2 = Videogame.create(name="Test2", price=20, releasedate=datetime.now(), genre="RPG", sid=2000)

        self.game_1.in_creation = False
        self.game_2.in_creation = False

        self.game_1.save()
        self.game_2.save()

    def test_cart_add_game(self):
        response = self.client.get(reverse('cart'))
        self.assertEqual(response.status_code, 302,
                         "L'utente non autenticato non ha un carrello, dunque deve essere reindirizzato")
        self.client.force_login(self.user, backend=None)
        response = self.client.get(reverse('cart'))
        self.assertEqual(response.status_code, 200, "L'utente autenticato può accedere al suo carrello normalmente")
        self.client.get(reverse('add-cart', kwargs={'name': self.game_1.name}))
        response = self.client.get(reverse('cart'))
        cart = response.context['user_cart']
        first_elem = cart.Videogames.all()[0]
        self.assertEqual(first_elem.name, 'Test1', 'Test1 dovrebbe essere aggiunto al carrello')

    def test_cart_add_game_you_own(self):
        self.client.force_login(self.user, backend=None)
        response = self.client.get(reverse('cart'))
        self.user.library.add_videogame(self.game_1)
        self.client.get(reverse('add-cart', kwargs={'name': self.game_1.name}))
        response = self.client.get(reverse('cart'))
        cart = response.context['user_cart']
        self.assertQuerysetEqual(cart.Videogames.all(), [],
                                 'Test1 non dovrebbe essere aggiunto al carrello, perché già acquistato')

    def test_cart_add_game_already_in_cart(self):
        self.client.force_login(self.user, backend=None)
        self.user.shoppingcart.add_videogame(self.game_1)
        self.client.get(reverse('add-cart', kwargs={'name': self.game_1.name}))
        response = self.client.get(reverse('cart'))
        cart = response.context['user_cart']
        self.assertEqual(len(cart.Videogames.all()), 1,
                         'Dovrebbe esserci solo 1 gioco nel carrello, perché quello che si sta aggiungendo è già presente')
        self.client.get(reverse('add-cart', kwargs={'name': self.game_2.name}))
        response = self.client.get(reverse('cart'))
        cart = response.context['user_cart']
        self.assertEqual(len(cart.Videogames.all()), 2, 'Dovrebbero esserci 2 giochi')

    def test_cart_remove_game(self):
        self.client.force_login(self.user, backend=None)
        self.client.get(reverse('add-cart', kwargs={'name': self.game_1.name}))
        self.client.get(reverse('remove-cart', kwargs={'name': self.game_1.name}))
        response = self.client.get(reverse('cart'))
        cart = response.context['user_cart']
        self.assertQuerysetEqual(cart.Videogames.all(), [],
                                 'Il carrello dovrebbe essere vuoto')
        response = self.client.get(reverse('remove-cart', kwargs={'name': self.game_1.name}))
        self.assertEqual(response.status_code, 302,
                         'Anche in caso di rimozione di un gioco non presente in carrello dovrebbe reindirizzare senza errori')
        response = self.client.get(reverse('cart'))
        cart = response.context['user_cart']
        self.assertQuerysetEqual(cart.Videogames.all(), [],
                                 'Il carrello dovrebbe essere vuoto')

    def test_checkout(self):
        self.client.force_login(self.user, backend=None)
        self.assertQuerysetEqual(self.user.library.getVideogames(), [],
                                 'Un nuovo utente dovrebbe avere la libreria vuota')
        self.client.get(reverse('add-cart', kwargs={'name': self.game_1.name}))
        response = self.client.get(reverse('checkout'))
        self.assertEqual(response.status_code, 302, 'Non dovrebbe essere possibile eseguire un checkout tramite get,'
                                                    ' l\' utente dovrebbe essere reindirizzato')
        response = self.client.post(reverse('checkout'))
        self.assertEqual(response.status_code, 200, 'Se il checkout viene eseguito con una post, '
                                                    'il checkout dovrebbe andare a buon fine')
        self.assertEqual(self.user.library.getVideogames()[0].name, 'Test1',
                         'Dopo il checkout il gioco aggiunto dovrebbe'
                         'essere aggiunto alla libreria')
