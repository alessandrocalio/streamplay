import datetime

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm

from user_management.models import User, PrivacyField


class RegistrationForm(UserCreationForm):
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta:
        model = User
        fields = (
            "name", "surname", "birthday", "email", "phone_number", "nationality", "username", "password1", "password2")
        widgets = {'name': forms.TextInput(attrs={'class': 'form-control'}),
                   'surname': forms.TextInput(attrs={'class': 'form-control'}),
                   'birthday': forms.TextInput(attrs={'class': 'form-control'}),
                   'email': forms.TextInput(attrs={'class': 'form-control'}),
                   'phone_number': forms.TextInput(attrs={'class': 'form-control'}),
                   'nationality': forms.Select(attrs={'class': 'form-control'}),
                   'username': forms.TextInput(attrs={'class': 'form-control'}),
                   }

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'registration-crispy-form'
        self.helper.add_input(Submit('submit', 'Submit'))

    def clean(self):
        if self.is_valid():
            if self.cleaned_data['username'].__contains__("/"):
                raise forms.ValidationError("Usernames with '/' character are not allowed")
            if self.cleaned_data['birthday']>datetime.date.today():
                raise forms.ValidationError("Welcome John Titor")
            if User.objects.filter(email=self.cleaned_data['email']).exists():
                raise forms.ValidationError("Email already exists")
            if User.objects.filter(username=self.cleaned_data['username']).exists():
                raise forms.ValidationError("Username already exists")



class UserAuthenticationForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'type': 'password', 'placeholder': 'Password', 'id': 'inputPassword'}))

    class Meta:
        model = User
        fields = ('email', 'password')
        widgets = {'email': forms.TextInput(
            attrs={'class': 'form-control', 'type': 'email', 'placeholder': 'Email Address', 'id': 'inputEmail'}),
        }

    def clean(self):
        if self.is_valid():
            email = self.cleaned_data['email']
            password = self.cleaned_data['password']
            if not authenticate(email=email, password=password):
                raise forms.ValidationError("Invalid login: check your email or password")


class PrivacyForm(forms.ModelForm):
    class Meta:
        model = PrivacyField
        fields = ['show_email','show_library','show_birthday','show_name','show_surname']


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['name','surname','nationality','picture','birthday','bio']