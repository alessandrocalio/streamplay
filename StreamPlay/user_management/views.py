from compat import render_to_string
from django.contrib.auth import login, authenticate
from django.contrib.auth import logout
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.db.models import Q
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic import DetailView
from django.views.generic.list import MultipleObjectMixin

from StreamPlay.token import account_activation_token
from orders.models import Library
from orders.views import ShoppingCart
from user_management.forms import RegistrationForm, UserAuthenticationForm, UserForm, PrivacyForm
from user_management.models import User, PrivacyField


def registration(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            mail_subject = 'Activate your StreamPlay account.'
            message = render_to_string('user_management/acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(
                mail_subject, message, to=[to_email]
            )
            email.send()
            return render(request, 'user_management/new_registration_mail_sent.html')
    else:
        form = RegistrationForm()
    return render(request, 'user_management/registration.html', {'registration_form': form})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(id=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()

        shopping_cart = ShoppingCart.create(userid=user)
        shopping_cart.save()

        library = Library.create(userid=user)
        library.save()

        privacy = PrivacyField.create(userid=user)
        privacy.save()

        login(request, user)

        return redirect('user-details', user.username)
    else:
        return HttpResponse('Activation link is invalid!')


def loginview(request):
    context = {}

    user = request.user
    if user.is_authenticated:
        return redirect("profile/" + user.username)

    if request.POST:
        form = UserAuthenticationForm(request.POST)
        if form.is_valid():
            email = request.POST['email']
            password = request.POST['password']
            user = authenticate(email=email, password=password)

            if user:
                login(request, user)
                return redirect("profile/" + user.username)

    else:
        form = UserAuthenticationForm()

    context['login_form'] = form
    return render(request, 'user_management/login.html', context)


def logoutview(request):
    logout(request)
    return redirect('login')


class Profile(DetailView, MultipleObjectMixin):
    model = User
    fields = ['name', 'surname', 'username', 'picture']
    template_name = 'user_management/profile.html'
    slug_field = 'username'

    def get_context_data(self, **kwargs):
        library = Library.objects.get(UserId=self.object.id).getVideogames().order_by('name')
        context = super(Profile, self).get_context_data(object_list=library, **kwargs)
        privacy = PrivacyField.objects.get(user=self.object.id)
        context['privacy'] = privacy
        return context


def UpdateProfile(request):
    privacy_fields = PrivacyField.objects.get(user_id=request.user.id)
    if request.POST:
        user_form = UserForm(request.POST, files=request.FILES, instance=request.user)
        privacy_form = PrivacyForm(request.POST, instance=privacy_fields)
        if user_form.is_valid() and privacy_form.is_valid():
            user_form.save()
            privacy_form.save()
            request.user.save()
            privacy_fields.save()
            print(request.POST.get('picture'))
            return HttpResponseRedirect(reverse('user-details', kwargs={'slug': request.user.username}))
        else:
            user_form = UserForm(instance=request.user)
            privacy_form = PrivacyForm(instance=privacy_fields)
    else:
        user_form = UserForm(instance=request.user)
        privacy_form = PrivacyForm(instance=privacy_fields)

    return render(request, "user_management/update_profile.html",
                  {'user_form': user_form, 'privacy_form': privacy_form})


def password_reset_request(request):
    if request.method == "POST":
        password_reset_form = PasswordResetForm(request.POST)
        if password_reset_form.is_valid():
            data = password_reset_form.cleaned_data['email']
            associated_users = User.objects.filter(Q(email=data) | Q(username=data))
            if associated_users.exists():
                for user in associated_users:
                    subject = "Password Reset Requested"
                    email_template_name = "user_management/password_reset_email.html"

                    c = {
                        "email": user.email,
                        'domain': '127.0.0.1:8000',
                        'site_name': 'Website',
                        "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                        "user": user,
                        'token': default_token_generator.make_token(user),
                        'protocol': 'http',
                    }
                    message = render_to_string(email_template_name, c)
                    print(message)
                    print(user.email)
                    email = EmailMessage(subject, message, to=[user.email])
                    email.send()
                    return redirect("password_reset_done")
    password_reset_form = PasswordResetForm()
    if request.user.is_authenticated:
        subject = "Password Reset Requested"
        email_template_name = "user_management/password_reset_email.html"

        c = {
            "email": request.user.email,
            'domain': '127.0.0.1:8000',
            'site_name': 'Website',
            "uid": urlsafe_base64_encode(force_bytes(request.user.pk)),
            "user": request.user,
            'token': default_token_generator.make_token(request.user),
            'protocol': 'http',
        }
        message = render_to_string(email_template_name, c)
        email = EmailMessage(subject, message, to=[request.user.email])
        email.send()
        return redirect("password_reset_done")

    return render(request=request, template_name="user_management/password_reset_html.html",
                  context={"password_reset_form": password_reset_form})
