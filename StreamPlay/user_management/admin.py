from django.contrib import admin
from django.contrib.auth.models import Group

# Register your models here.
from orders.models import Library
from user_management.models import User

admin.site.register(User)
admin.site.register(Library)
admin.site.unregister(Group)
