from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.urls import reverse
from django_countries.fields import CountryField
from phonenumber_field.modelfields import PhoneNumberField


class MyAccountManager(BaseUserManager):

    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError("User must have an email address")
        if not username:
            raise ValueError("User must have an username")

        user = self.model(
            email=self.normalize_email(email),
            username=username
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):

        user = self.create_user(
            email=self.normalize_email(email),
            username=username,
            password=password,
        )

        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    email = models.EmailField(max_length=60, unique=True)
    username = models.CharField(max_length=30, unique=True)
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    birthday = models.DateField(null=True)
    phone_number = PhoneNumberField(blank=True)
    nationality = CountryField()
    picture = models.ImageField(upload_to='profile_image', blank=True)
    bio = models.TextField(blank=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']
    objects = MyAccountManager()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def get_absolute_url(self):
        return reverse('user-details', args=[self.username])

    @classmethod
    def create(cls, email, username):
        return User(email=email, username=username, name="Dummy", surname="User")


class PrivacyField(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    show_name = models.BooleanField(default=True)
    show_surname = models.BooleanField(default=True)
    show_email = models.BooleanField(default=True)
    show_birthday = models.BooleanField(default=True)
    show_library = models.BooleanField(default=True)

    @classmethod
    def create(cls, userid):
        privacy_field = cls(user=userid)
        return privacy_field
