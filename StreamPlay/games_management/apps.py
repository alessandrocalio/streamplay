from django.apps import AppConfig


class GamesManagementConfig(AppConfig):
    name = 'games_management'
