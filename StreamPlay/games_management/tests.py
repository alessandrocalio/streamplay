from datetime import datetime

from django.test import TestCase

from orders.models import ShoppingCart
from raccomander_systems.reviews_filter.videogame_by_reviews import create_model, fit_algo, get_predicted_rate
from user_management.models import User
from .models import Videogame, Review


class VideogameTest(TestCase):

    def testDiscountedPrice(self):
        """"
        Test DIscounted Price
        """
        v = Videogame.create(name="Test", price=10, releasedate=datetime.now(), genre="RPG", sid=1000)
        v.on_sale = True
        v.sale_percentage = 10
        self.assertEqual(v.get_discounted_price(), 9)


class ShoppingCartTest(TestCase):

    def testSubTotal(self):
        a = User()
        a.save()

        sc = ShoppingCart.create(a)

        game_1 = Videogame.create(name="Test1", price=10, releasedate=datetime.now(), genre="RPG", sid=1000)
        game_2 = Videogame.create(name="Test2", price=20, releasedate=datetime.now(), genre="RPG", sid=2000)
        game_3 = Videogame.create(name="Test3", price=30, releasedate=datetime.now(), genre="RPG", sid=3000)
        game_1.in_creation = False
        game_2.in_creation = False
        game_3.in_creation = False
        game_1.save()

        game_2.save()
        game_3.save()

        sc.save()

        sc.add_videogame_o(game_1)
        sc.add_videogame_o(game_2)
        sc.add_videogame_o(game_3)

        self.assertEqual(sc.sub_total(), 60)


class CollaborativeFilteringTest(TestCase):
    '''
    We test how the predicted ratings fluctuate based on similar users
    User 1 , User 2 and User 3 have equal ratings for 5 dummy games
    User 1 rated last game with a rate=5
    User 3 rated last game with a rate=1
    User 2 didn't rate last game
    The predicted rate for tha last game for the User 2 should be the mean of the rating of the most similar users ,
    in this case almost 3
    '''

    def test(self):
        user_1 = User.objects.create(username="user_1", email="user_1@gmail.com")
        user_2 = User.objects.create(username="user_2", email="user_2@gmail.com")
        user_3 = User.objects.create(username="user_3", email="user_3@gmail.com")

        user_1.save()
        user_2.save()

        game_1 = Videogame.create(name="Test1", price=10, releasedate=datetime.now(), genre="RPG", sid=1000)
        game_2 = Videogame.create(name="Test2", price=20, releasedate=datetime.now(), genre="RPG", sid=2000)
        game_3 = Videogame.create(name="Test3", price=30, releasedate=datetime.now(), genre="RPG", sid=3000)
        game_4 = Videogame.create(name="Test4", price=40, releasedate=datetime.now(), genre="RPG", sid=4000)
        game_5 = Videogame.create(name="Test5", price=50, releasedate=datetime.now(), genre="RPG", sid=5000)
        game_6 = Videogame.create(name="Test6", price=60, releasedate=datetime.now(), genre="RPG", sid=6000)

        game_1.in_creation = False
        game_2.in_creation = False
        game_3.in_creation = False
        game_4.in_creation = False
        game_5.in_creation = False
        game_6.in_creation = False

        game_1.save()
        game_2.save()
        game_3.save()
        game_4.save()
        game_5.save()
        game_6.save()

        review_1_user_1 = Review.create(user_1, game_1, 1, "test")
        review_2_user_1 = Review.create(user_1, game_2, 2, "test")
        review_3_user_1 = Review.create(user_1, game_3, 3, "test")
        review_4_user_1 = Review.create(user_1, game_4, 4, "test")
        review_5_user_1 = Review.create(user_1, game_5, 5, "test")
        review_6_user_1 = Review.create(user_1, game_6, 1, "test")

        review_1_user_2 = Review.create(user_2, game_1, 1, "test")
        review_2_user_2 = Review.create(user_2, game_2, 2, "test")
        review_3_user_2 = Review.create(user_2, game_3, 3, "test")
        review_4_user_2 = Review.create(user_2, game_4, 4, "test")
        review_5_user_2 = Review.create(user_2, game_5, 5, "test")

        review_1_user_3 = Review.create(user_3, game_1, 1, "test")
        review_2_user_3 = Review.create(user_3, game_2, 2, "test")
        review_3_user_3 = Review.create(user_3, game_3, 3, "test")
        review_4_user_3 = Review.create(user_3, game_4, 4, "test")
        review_5_user_3 = Review.create(user_3, game_5, 5, "test")
        review_6_user_3 = Review.create(user_3, game_6, 5, "test")

        review_1_user_1.save()
        review_2_user_1.save()
        review_3_user_1.save()
        review_4_user_1.save()
        review_5_user_1.save()
        review_6_user_1.save()

        review_1_user_2.save()
        review_2_user_2.save()
        review_3_user_2.save()
        review_4_user_2.save()
        review_5_user_2.save()

        review_1_user_3.save()
        review_2_user_3.save()
        review_3_user_3.save()
        review_4_user_3.save()
        review_5_user_3.save()
        review_6_user_3.save()

        create_model()
        fit_algo()

        self.assertAlmostEqual(get_predicted_rate(user_2.id, game_6.id), 3, delta=0.5)
