from django.contrib import admin

from games_management.models import Videogame, Review

admin.site.register(Videogame)
admin.site.register(Review)
