import datetime

from background_task import background
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import Avg
from django.dispatch import receiver

from user_management.models import User


class Videogame(models.Model):
    name = models.CharField(unique=True, max_length=64)
    price = models.DecimalField(max_digits=6, decimal_places=2, validators=[MinValueValidator(1)])

    GENREOPTIONS = (
        ("Action", "Action"),
        ("Indie", "Indie"),
        ("Strategy", "Strategy"),
        ("RPG", "RPG"),
        ("Animation & Modeling", "Animation & Modeling"),
        ("Casual", "Casual"),
        ("Simulation", "Simulation"),
        ("Racing", "Racing"),
        ("Adventure", "Adventure"),
        ("Violent", "Violent"),
        ("Nudity", "Nudity"),
        ("Free to Play", "Free to Play"),
        ("Sports", "Sports"),
        ("Gore", "Gore"),
        ("Massively Multiplayer", "Massively Multiplayer"),
        ("Utilities", "Utilities"),
        ("Design & Illustration", "Design & Illustration"),
        ("Education", "Education"),
        ("Web Publishing", "Web Publishing"),
        ("Sexual Content", "Sexual Content"),
        ("Audio Production", "Audio Production"),
        ("Photo Editing", "Photo Editing"),
        ("Early Access", "Early Access"),
    )
    genre = models.CharField(max_length=25, choices=GENREOPTIONS)
    sid = models.IntegerField(default=0, null=True, blank=True)
    description = models.TextField(max_length=5000)
    release_date = models.DateField()
    on_sale = models.BooleanField(default=False)
    on_sale_last_date = models.DateField(blank=True, null=True, default=None)
    sale_percentage = models.DecimalField(max_digits=2, decimal_places=0, validators=[MinValueValidator(1)], blank=True,
                                          null=True, default=None)
    picture = models.ImageField(upload_to='')

    image_1 = models.ImageField(upload_to='', default=None, null=True, blank=True)
    image_2 = models.ImageField(upload_to='', default=None, null=True, blank=True)
    image_3 = models.ImageField(upload_to='', default=None, null=True, blank=True)

    avg_review = models.DecimalField(max_digits=1, decimal_places=0,
                                     validators=[MinValueValidator(1), MaxValueValidator(5)], default=1)

    in_creation = models.BooleanField(default=True, editable=False)

    class Meta:
        verbose_name = 'Videogame'
        verbose_name_plural = 'Videogames'

    @classmethod
    def create(cls,name,price,releasedate,genre,sid):
        return cls(name=name,price=price,release_date=releasedate,genre=genre,sid=sid)


    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f"videogame_details/{self.name}"

    def get_discounted_price(self):
        if self.on_sale:
            return (self.price * (100 - int(self.sale_percentage))) / 100

    @background
    def remove_discount(videogameId):
        v = Videogame.objects.get(id=videogameId)
        v.sale_percentage = 0
        v.on_sale = False
        v.on_sale_last_date = None
        v.save()

    def update_rating(self):
        new_avg_rate = Review.objects.filter(Videogame=self.id).aggregate(Avg('rate'))
        self.avg_review = new_avg_rate['rate__avg']
        self.save()

    def save(self, *args, **kwargs):
        # Each time a game is created we need to re-do the model based on their description for advice similar games
        if self.in_creation:
            from raccomander_systems.description_based_approach.videogames_by_orders import create_model
            self.__in_creation = False
            create_model()

        # When the game become in sale we schedule the remove_discount function that will be run in the on_sale_last_date at midnight
        if (self.on_sale):
            date = datetime.datetime(self.on_sale_last_date.year, self.on_sale_last_date.month,
                                     self.on_sale_last_date.day)
            self.remove_discount(self.id, schedule=date)

        old = Videogame.objects.filter(id=getattr(self, "id", None)).first()

        super().save(*args, **kwargs)
        if old:
            if (old.description != self.description or old.name != self.name):
                from raccomander_systems.description_based_approach.videogames_by_orders import create_model
                create_model()





class Review(models.Model):
    UserId = models.ForeignKey(User, on_delete=models.CASCADE)
    Videogame = models.ForeignKey(Videogame, on_delete=models.CASCADE)
    rate = models.DecimalField(max_digits=1, decimal_places=0,
                               validators=[MinValueValidator(1), MaxValueValidator(5)], default=0)
    comment = models.TextField(max_length=1024)

    def __str__(self):
        return self.UserId.email + " - " + self.Videogame.name

    @classmethod
    def create(cls, userid, videogame, rate, comment):
        review = cls(UserId=userid)
        review.Videogame = videogame
        review.rate = rate
        review.comment = comment
        return review

    class Meta:
        unique_together = (('UserId', 'Videogame'),)
