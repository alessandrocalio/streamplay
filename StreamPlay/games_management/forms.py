from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django_starfield import *


class SearchForm(forms.Form):
    OPTIONS = (
        ("-", "-"),
        ("Action", "Action"),
        ("Indie", "Indie"),
        ("Strategy", "Strategy"),
        ("RPG", "RPG"),
        ("Animation & Modeling", "Animation & Modeling"),
        ("Casual", "Casual"),
        ("Simulation", "Simulation"),
        ("Racing", "Racing"),
        ("Adventure", "Adventure"),
        ("Violent", "Violent"),
        ("Nudity", "Nudity"),
        ("Free to Play", "Free to Play"),
        ("Sports", "Sports"),
        ("Gore", "Gore"),
        ("Massively Multiplayer", "Massively Multiplayer"),
        ("Utilities", "Utilities"),
        ("Design & Illustration", "Design & Illustration"),
        ("Education", "Education"),
        ("Web Publishing", "Web Publishing"),
        ("Sexual Content", "Sexual Content"),
        ("Audio Production", "Audio Production"),
        ("Photo Editing", "Photo Editing"),
        ("Early Access", "Early Access"),
    )

    REVIEW_OPTION = (
        ("1", "1"),
        ("2", "2"),
        ("3", "3"),
        ("4", "4"),
        ("5", "5"),

    )

    name = forms.CharField(required=False)
    genre = forms.CharField(required=False, widget=forms.Select(choices=OPTIONS))
    price_from = forms.DecimalField(required=False, min_value=0, max_value=1000, max_digits=3)
    price_to = forms.DecimalField(required=False, min_value=0, max_value=1000, max_digits=3)
    on_sale = forms.BooleanField(required=False)
    avg_review = forms.DecimalField(required=False, min_value=0, max_value=5, max_digits=3,
                                    widget=forms.Select(choices=REVIEW_OPTION))

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'search-crispy-form'
        self.helper.add_input(Submit('submit', 'Search'))


class ReviewForm(forms.Form):
    rating = forms.IntegerField(required=True, widget=Stars, initial=1)
    your_review = forms.CharField(required=False, max_length=1024,
                                  widget=forms.Textarea(attrs={'cols': 100, 'rows': 10}))

    def __init__(self, *args, **kwargs):
        super(ReviewForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'review-crispy-form'
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))
