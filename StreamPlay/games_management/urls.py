from django.contrib.admin.views.decorators import staff_member_required
from django.urls import path

from games_management import views

urlpatterns = [

    path('videogame_details/<slug>', views.videogame_details.as_view(), name='videogame-details'),
    path('update_videogame/<slug>', staff_member_required(views.UpdateVideogame.as_view()), name='update-videogame'),
    path('delete_videogame/<slug>', staff_member_required(views.DeleteVideogame.as_view()), name='delete-videogame'),
    path('', views.VideogameList, name='videogame-list'),
    path('upload_review/<str:name>', views.upload_review, name='upload-review'),
    path('update_review/<str:name>', views.update_review, name='update-review'),
    path('jsonVideogames', views.jsonVideogame, name='jsonVideogames'),


]
