import json

from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import DetailView, UpdateView, DeleteView

from games_management.forms import ReviewForm
from games_management.models import Videogame, Review
from orders.models import ShoppingCart, Library
from raccomander_systems.reviews_filter.videogame_by_reviews import create_model, fit_algo


class videogame_details(DetailView):
    model = Videogame
    fields = ['name', 'price',
              'description', 'release_date',
              'on_sale', 'on_sale_last_date',
              'sale_percentage']
    template_name = 'games_management/videogame_details.html'
    slug_field = 'name'

    def get_context_data(self, **kwargs, ):
        context = super(videogame_details, self).get_context_data(**kwargs)

        # Se il gioco è posseduto dal current user
        if Library.objects.filter(UserId=self.request.user.id, Videogames__name=self.object.name):
            is_owned = True
        else:
            is_owned = False

        # Se il gioco è recensito dal current user
        if Review.objects.filter(UserId=self.request.user.id, Videogame=self.object.id):
            review = Review.objects.get(UserId=self.request.user.id, Videogame=self.object.id)
            reviewed = True
            context['your_review'] = review
        else:
            reviewed = False
            context['your_review'] = None

        # Se il gioco è attualmente nel carrello del current user
        if ShoppingCart.objects.filter(UserId=self.request.user.id, Videogames__name=self.object.name):
            in_cart = True
        else:
            in_cart = False

        # Recensioni degli utenti del gioco ( Tranne la recensione del current user)
        context['reviews'] = Review.objects.filter(Videogame__name=self.object.name).exclude(
            UserId=self.request.user.id)
        context['is_owned'] = is_owned
        context['is_reviewed'] = reviewed
        context['in_cart'] = in_cart
        if reviewed:
            context['review_form'] = ReviewForm(initial={'rating': review.rate, 'your_review': review.comment})
        else:
            context['review_form'] = ReviewForm
        return context


class UpdateVideogame(UpdateView):
    model = Videogame
    template_name = 'games_management/update_videogame.html'
    slug_field = 'name'
    fields = ['name', 'price', 'genre', 'description', 'release_date',
              'on_sale', 'on_sale_last_date', 'sale_percentage', 'picture', 'image_1', 'image_2', 'image_3']

    def get_success_url(self):
        return reverse('videogame-details', kwargs={'slug': self.object.name})


def VideogameList(request):
    context = {}
    if request.method == 'POST':
        context['initial_name'] = request.POST.get('name')
    else:
        context['initial_name'] = ""
    template_name = 'games_management/videogame_list2.html'
    return render(request, template_name, context)


def upload_review(request, name):
    if request.method == "GET":
        return redirect('videogame-list')
    videogame_to_review = Videogame.objects.get(name=name)
    review = Review.create(userid=request.user,
                           videogame=videogame_to_review,
                           rate=request.POST["rating"],
                           comment=request.POST["your_review"])
    review.save()
    videogame_to_review.update_rating()

    # update review model e calcolo della similarità
    create_model()
    fit_algo()

    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


def update_review(request, name):
    if request.method == "GET":
        return redirect('videogame-list')
    videogame_to_review = Videogame.objects.get(name=name)

    review = Review.objects.get(UserId=request.user.id, Videogame__name=name)
    review.rate = request.POST["rating"]
    review.comment = request.POST["your_review"]

    review.save()
    videogame_to_review.update_rating()
    # update review model e calcolo della similarità
    create_model()
    fit_algo()
    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


def jsonVideogame(request):
    object_list = Videogame.objects.all()
    serialized_objects = serializers.serialize('json', object_list)
    json_objects = json.loads(serialized_objects)
    list_objects = []
    for index in range(len(json_objects)):
        list_objects.append(json_objects[index]['fields'])

    for item in list_objects:
        item.pop("description")
        item.pop("on_sale_last_date")
    return HttpResponse(json.dumps(list_objects), content_type='application/json;charset=utf-8')


class DeleteVideogame(DeleteView):
    model = Videogame
    slug_field = 'name'

    def get_success_url(self):
        from raccomander_systems.description_based_approach.videogames_by_orders import create_model
        create_model()
        return reverse('videogame-list')
