import pandas as pd
from django.forms import model_to_dict
from surprise import Dataset, accuracy
from surprise import SVD, Reader, KNNWithMeans, SVDpp, SlopeOne, NMF, NormalPredictor, KNNBaseline, KNNBasic, \
    KNNWithZScore, BaselineOnly, CoClustering
from surprise.model_selection import cross_validate, KFold, GridSearchCV

from games_management.models import Review


def get_data():
    # Dizionario contenente tutte le reviews {'userid' , 'videogameid' , 'rate' }
    reviews_dict = {}

    review_list = Review.objects.all()
    # Ogni review viene parsata in un dizionario

    for review in review_list:
        review_dict = model_to_dict(review, fields=['UserId', 'Videogame', 'rate'])
        for k, v in review_dict.items():
            reviews_dict.setdefault(k, []).append(v)

    df = pd.DataFrame(reviews_dict)
    print(df)
    reader = Reader(rating_scale=(1, 5))
    # Parsing del dizionario in un Dataset di panda
    data = Dataset.load_from_df(df[["UserId", "Videogame", "rate"]], reader)
    return data


def test_all_algorithm():
    data = get_data()
    benchmark = []
    # Iterate over all algorithms

    for algorithm in [SVD(), SVDpp(), SlopeOne(), NMF(), NormalPredictor(), KNNBaseline(), KNNBasic(), KNNWithMeans(),
                      KNNWithZScore(), BaselineOnly(), CoClustering()]:
        # Perform cross validation
        results = cross_validate(algorithm, data, measures=['RMSE'], cv=3, verbose=False)

        # Get results & append algorithm name
        tmp = pd.DataFrame.from_dict(results).mean(axis=0)
        tmp = tmp.append(pd.Series([str(algorithm).split(' ')[0].split('.')[-1]], index=['Algorithm']))
        benchmark.append(tmp)

    pd.DataFrame(benchmark).set_index('Algorithm').sort_values('test_rmse')
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        print(benchmark)


def testalgo():
    data = get_data()
    param_grid = {'k': [1,2,3,5,10],
                  'sim_options': {'name': ['msd'],
                                  'min_support': [5],
                                  'user_based': [True]}
                  }
    gs = GridSearchCV(KNNWithZScore, param_grid, measures=['rmse', 'mae'], cv=3)
    gs.fit(data)
    # best RMSE score
    print(gs.best_score['rmse'])

    # combination of parameters that gave the best RMSE score
    print(gs.best_params['rmse'])

    kf = KFold(n_splits=3)
    algo = KNNWithZScore()
    for trainset, testset in kf.split(data):
        # train and test algorithm.
        algo.fit(trainset)
        predictions = algo.test(testset)
        # Compute and print Root Mean Squared Error
        rmse = accuracy.rmse(predictions, verbose=True)
