import pickle

import pandas as pd
from django.forms.models import model_to_dict
from surprise import Dataset, KNNWithZScore
from surprise import Reader

from games_management.models import Review
algo = KNNWithZScore(k=5,sim_options={'name': 'msd', 'user_based': False})

pkl_filename = "prediction_model.pkl"


def create_model():
    # Dizionario contenente tutte le reviews {'userid' , 'videogameid' , 'rate' }
    reviews_dict = {}

    review_list = Review.objects.all()
    # Ogni review viene parsata in un dizionario

    for review in review_list:
        review_dict = model_to_dict(review, fields=['UserId', 'Videogame', 'rate'])
        for k, v in review_dict.items():
            reviews_dict.setdefault(k, []).append(v)


    df = pd.DataFrame(reviews_dict)
    reader = Reader(rating_scale=(1, 5))
    #  Parsing del dizionario in un Dataset di panda
    data = Dataset.load_from_df(df[["UserId", "Videogame", "rate"]], reader)
    trainingSet = data.build_full_trainset()
    with open(pkl_filename, 'wb') as file:
        pickle.dump(trainingSet, file)

def get_predicted_rate(userId, videogameId):
    return algo.predict(userId, videogameId).est


def fit_algo():
    with open(pkl_filename, 'rb') as file:
        pickle_model = pickle.load(file)
    algo.fit(pickle_model)
