from games_management.models import Videogame
from orders.models import Library
from raccomander_systems.description_based_approach.videogames_by_orders import get_videogames as get_videogames_by_o
from raccomander_systems.reviews_filter import videogame_by_reviews as vr


def get_videogames(userId):
    similar_games = get_videogames_by_o(userId=userId)  # lists of list of tuple
    flat_list = [list(item) for sublist in similar_games for item in sublist]  # list of lists

    for videogame in flat_list:
        videogameId = Videogame.objects.get(name=videogame[0]).id
        predicted_rate = vr.get_predicted_rate(userId=userId, videogameId=videogameId)
        videogame[1] = videogame[1] * predicted_rate

    clean_dictionary = {}
    for videogame in flat_list:
        if videogame[0] in clean_dictionary.keys():  # Se il nome è presente nelle key del dizioanrio
            if videogame[1] > clean_dictionary.get(
                    videogame[0]):  # se il rate del videogioco è maggiore di quello nel dizionario
                clean_dictionary[videogame[0]] = videogame[1]  # si setta il maggiore
            else:
                pass
        clean_dictionary[videogame[0]] = videogame[1]

    sorted_videogame = sorted(clean_dictionary.items(), key=lambda x: x[1], reverse=True)
    videogameObjects = []
    library = Library.objects.get(UserId=userId).getVideogames()
    for videogame in sorted_videogame:
        instance = Videogame.objects.get(name=videogame[0])
        if instance not in library:
            videogameObjects.append(instance)
    return videogameObjects
