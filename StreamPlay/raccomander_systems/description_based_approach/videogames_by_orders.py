import gensim
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from gensim.parsing.preprocessing import preprocess_string

from games_management.models import Videogame
from orders.models import Library

'''
Create a vectorial model based on the videogame's description to compute the cosine similarity.
Used to advice new games similar to the previously bought games
'''


def create_model():
    games = tuple(Videogame.objects.all().values_list('name', 'description'))

    tagged_data = []
    for game in games:
        content_filtered = preprocess_string(str(game[1])) + preprocess_string(game[0])
        tagged_document = TaggedDocument(words=content_filtered, tags=[game[0]])
        tagged_data.append(tagged_document)

    for data in tagged_data:
        print(data)

    params = {
        'vector_size': 300,  # Number of dimensions
        'workers': 8,  # Threading, set to number of CPU cores or less
        'epochs': 50
    }
    model = gensim.models.Doc2Vec(**params)

    model.build_vocab(tagged_data)

    model.train(tagged_data, total_examples=model.corpus_count, epochs=model.iter)

    model.save("videogame_vector.model")


'''
Given a user id it returns a list of similar games to the previously bought games,
Similar videogames is a list that contains n lists
each i-th list contains the 3 most similar videogames to the i-th game of the user's library
'''


def get_videogames(userId):
    model = Doc2Vec.load("videogame_vector.model")
    library = Library.objects.get(UserId=userId).getVideogames()
    similar_videogames = []
    suggestion_dict = dict()

    for game in library:
        three_most_similar_games = model.docvecs.most_similar(game.name)[:3]
        similar_videogames.append(three_most_similar_games)
        suggestion_dict[game.name] = three_most_similar_games

    return similar_videogames
