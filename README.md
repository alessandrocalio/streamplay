# StreamPlay

Progetto per l'esame di tecnologie web 

# Installazione

Il progetto utilizza python 3.8, pipenv come enviroment con django.

Scaricare python 3.8 e pip:

```
sudo apt-get install python3.8
```

Scaricare pipenv e django sul pipenv

```
pip3 install pipenv
pipenv install django
```

Le librerie utilizzate sono:

django_countries==6.1.2

Django==3.0.8

django_phonenumber_field==4.0.0

gensim==3.8.3

pandas==1.0.5

django_compat==1.0.15

django_background_tasks==1.2.5

django_starfield==1.0.post1

scikit_surprise==1.1.0

six==1.15.0

django_crispy_forms==1.9.2

surprise==0.1

easy-thumbnails==2.7

phonenumbers==8.12.6

Pillow==7.2.0

django-extensions==3.0.8

All'interno della repository si trova un file "requirements.txt" per installare velocemente tutte le dipendenze.

# NOTA SU "scikit_surprise":

Questa libreria richiede un compliatore c++ con supporto ad openMP.

Su distribuzioni linux (testato su distro linux debian based) dovrebbe essere sufficiente lanciare il comando 
```
pip3 install -r requirements.txt
``` 
all'interno dell'enviroment con django installato (N.B. la versione di python DEVE essere la versione 3.8, altrimenti l'installazione fallisce).

L'installazione richiede python3.8-dev, ma spesso è già incluso in python3.8.

Su windows il metodo più semplice per installare scikit_surprise è scaricare i build tools per visual studio 2019 (https://visualstudio.microsoft.com/it/downloads/) per poi installare i pacchetti all'interno di requirements.txt.

Pagina di scikit_surprise: http://surpriselib.com/

# Lanciare il sito

Per lanciare il server sono necessari due terminali

Terminale 1:

```
pipenv shell
```

```
python manage.py runserver
```

Terminale 2:
(Gestisce le task in backgroud)

```
pipenv shell
```

```
python manage.py process_tasks
```


# Lanciare i test

Per testare le funzioni:

'''
python manage.py test games_management
'''

Per testare la view del carrello:

'''
python manage.py test user_management
'''

# Credenziali dell'admin del sito

login: admin@admin.it

password: admin
